resource "aws_internet_gateway" "MUST_gateway" {
  vpc_id = "${aws_vpc.MUST_network.id}"
  
  tags = {
      Name = "MUST_gateway"
    }
}