output "availability_zone" {
    description = "Test if the variable for the availability zone is set correctly"

    value       = local.aws_availability_zone
}

output "Wazuh_manager_volume" {
    description = "See how much disk space the Wazuh manager requires (approximately)"

    value       = local.manager_volume
}

output "Wazuh_indexer_volume" {
    description = "See how much disk space the Wazuh indexer requires (approximately)"

    value       = local.indexer_volume
}

output "ec2_instances_ip" {
    description = "The public IP address assigned to the instance, if applicable. NOTE: If you are using an aws_eip with your instance, you should refer to the EIP's address directly and not use `public_ip` as this field will change after the EIP is attached"
    value       = [for instance in module.ec2_instance : "${instance.tags_all.Name}: ${instance.public_ip} - ${instance.private_ip}"]
}

output "ec2_instances_data" {
    description = "Relevant EC2 instance data in JSON format"
    value       = [for instance in module.ec2_instance : {
        "instance" = { 
        "name"              = "${instance.tags_all.Name}",
            "public_dns"    = "${instance.public_dns}",
            "public_ip"     = "${instance.public_ip}",
            "private_ip"    = "${instance.private_ip}",
            "ssh_command"   = "ssh -i '~/.ssh/MUST.pem' ubuntu@${instance.public_dns}"
            }
        }
    ]
}

output "ec2_instances_data_full" {
    description = "All EC2 instance data in RAW format"
    value       = [for instance in module.ec2_instance : "${instance}"]
}