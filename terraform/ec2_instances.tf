module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  # Loop over variable
  for_each = local.ec2_instances
  
  name = "${each.key}"

  availability_zone = local.aws_availability_zone

  ami                    = "${each.value.AMI}"
  instance_type          = "${each.value.Type}"
  key_name               = var.aws_key_name
  vpc_security_group_ids = [aws_security_group.MUST_security_group.id]
  subnet_id              = aws_subnet.MUST_subnet_1.id

  associate_public_ip_address = true
  monitoring             = true

  # Disk space
  enable_volume_tags = false
  root_block_device = [
    {
      encrypted   = true
      volume_type = "gp2"
      volume_size = each.value.Volume
      tags = {
        Name = "${each.key}-root-volume"
      }
    },
  ]
  
  tags = {
    Terraform   = "true"
    Environment = "dev"
    Name        = "${each.key}"
  }
}
