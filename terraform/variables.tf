variable "aws_access_key_id" {
    type = string
}
variable "aws_secret_access_key" {
    type = string
}
variable "aws_session_token" {
    type = string
}

variable "aws_region" {
    type = string
}

variable "aws_key_name" {
    type = string
}

variable "aws_monitored_servers" {
    type = string
}

variable "aws_monitored_workstations" {
    type = string
}

variable "aws_monitored_network_devices" {
    type = string
}

locals {

    aws_availability_zone   = "${var.aws_region}a"

    # The volume of the disk is either the calculated value, or the default value, whichever is larger
    default_volume          = 8  # GB
    manager_volume          = var.aws_monitored_servers * 0.1 + var.aws_monitored_workstations * 0.04 + var.aws_monitored_network_devices * 0.2
    indexer_volume          = var.aws_monitored_servers * 3.7 + var.aws_monitored_workstations * 1.5 + var.aws_monitored_network_devices * 7.4
    stack_volume            = local.manager_volume + local.indexer_volume

    ami = {
        ubuntu_22_04 = "ami-0557a15b87f6559cf"
    }

    ec2_instances           = {
        MUST_Ansible = {
            Type    = "t2.micro"
            Volume  = local.default_volume
            AMI     = local.ami.ubuntu_22_04
        }
        # MUST_Wazuh_manager = {
        #     Type    = "t2.medium"
        #     Volume  = local.manager_volume > local.default_volume ? local.manager_volume : local.default_volume
        # }
        # MUST_Wazuh_indexer = {
        #     Type    = "t2.medium"
        #     Volume  = local.indexer_volume > local.default_volume ? local.indexer_volume : local.default_volume
        # }
        # MUST_Wazuh_dashboard = {
        #     Type    = "t2.medium"
        #     Volume  = local.default_volume
        # }
        MUST_Wazuh_agent = {
            Type    = "t2.micro"
            Volume  = local.default_volume
            AMI     = local.ami.ubuntu_22_04
        }
        MUST_Wazuh_stack = {
            Type    = "t2.large"
            Volume  = local.stack_volume > local.default_volume ? local.stack_volume : local.default_volume
            AMI     = local.ami.ubuntu_22_04
        }
        MUST_Backup = {
            Type    = "t2.micro"
            Volume  = local.default_volume
            AMI     = local.ami.ubuntu_22_04
        }
    }
}