# Terraform infrastructure 

The infrastructure is declared in several Terraform-files in the [terraform](/terraform/) directory. These files are:
* [variables.tf](#variablestf)
* [main.tf](#maintf)
* [vpc.tf](#vpctf)
* [subnets.tf](#subnetstf)
* [security.tf](#securitytf)
* [gateways.tf](#gatewaystf)
* [ec2_instances.tf](#ec2_instancestf)
* [output.tf](#outputtf)

Do note that while these files are inteded to be executed through the [GitLab pipeline](/README.md#pipeline),
they can also be executed locally through terraform commands.  
To ensure that the right variables are used, create a `.env` file in the root directory of this repository.  
For instructions on the syntax of this directory, see [.env.example](/.env.example).  
Once the `.env`-file has been created, run the [readEnv](/readEnv.ps1) script to transform the .env-variables into environment variables usable by Terraform.

Now, the infrastructure can be created and destroyed with the following commands:
```PowerShell
terraform init
terraform play
terraform apply
terraform apply -destroy
```

## variables.tf

This file simply "reads" the variables declared in [.gitlab-ci.yml](./.gitlab-ci.yml). Most of these variables simply refer to those declared in GitLab (see [GitLab setup](#gitlab-setup)).

Some local variables are also declared. First, some services require an avaiability zone. 
To ease this process, a variable is set which takes the first availability zone within the selected region (AWS_REGION).
For example, if the region is "us-east-1", the zone would be "us-east-1a".

Then, a variable named "ec2_instances" is declared. 
This variable stores infromation on the desired ec2 instanses, like the Ansible server or the Wazuh dashboard.
Every desired server also holds information on the desired instance type and disk space required (see [requirements](/requirements/)). It is formatted as follows:[^2]

```YAML
ec2_instances = {
    MUST_<server> = {
        Type    = "<AWS type>"
        Volume  = <required volume calculation> 
    }
}
```

## main.tf

In this file, AWS is set as a provider and the credentials set in variables.tf are used.
The backend is set to HTTP, which configures GitLab as the backend for the state files.

## vpc.tf

In this file, a [vpc](https://aws.amazon.com/vpc/) named "MUST_network" is created with subnet 10.10.0.0/16.

## subnets.tf

In this file, a subnet named "MUST_subnet_1" is created in the MUST VPC, with subnet address 10.10.1.0/28 (14 usable addresses).
Also, a route table is created and associated with the subnet to allow access through the internal gateway.

## security.tf

In this file, a security group is created. Several firewall rules are declared, corresponding to those required by Ansible and Wazuh, as stated in [/requirements](/requirements/Ansible.md).[^1]

## gateways.tf

This file, very simply, creates a gateway for internet access.

## ec2_instances.tf

In this file, the different EC2 instances declared in [variables.tf](variables.tf) are created.
They are all Ubuntu 22.04 LTS servers, have a public IP address and can be accessed over SSH.
Their type and disk space are set according to the values in [variables.tf](variables.tf).

## output.tf

This file is used to check variables and IP addresses and can be used for debugging.

# Known errors

[^1]: The cidr blocks in the ingress ports must allow either the specific IP's that you want to give access to the servers, or everyone.

[^2]: The volume calculation is done following the [Disk space requirements](/requirements/Wazuh.md).