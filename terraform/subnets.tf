resource "aws_subnet" "MUST_subnet_1" {
  vpc_id     = aws_vpc.MUST_network.id
  cidr_block = "10.10.1.0/28"
  availability_zone = local.aws_availability_zone

  tags = {
    Name = "MUST_subnet_1"
  }
}

resource "aws_route_table" "MUST_route_table" {
  vpc_id = "${aws_vpc.MUST_network.id}"
  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_internet_gateway.MUST_gateway.id}"
    }
  tags = {
      Name = "MUST_route_table"
    }
}

resource "aws_route_table_association" "subnet-association" {
  subnet_id      = "${aws_subnet.MUST_subnet_1.id}"
  route_table_id = "${aws_route_table.MUST_route_table.id}"
}