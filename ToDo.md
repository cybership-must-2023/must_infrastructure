# TO DO
 
## Terraform

## GitLab

- avoid using public IP's in public repo
- Extra:
    - get SAST to work

## Ansible

- set up Wazuh in a separate dashboard, indexer and manager instance

## Wazuh

- add additional [features](https://documentation.wazuh.com/current/user-manual/capabilities/index.html)
- fix the password script
- [Advanced API authentication](https://documentation.wazuh.com/current/user-manual/api/rbac/auth-context.html#authorization-context-method)
- Look up Wazuh setups and create functional rulesets and active responses 
- add [more handlers](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_handlers.html)

- backups en recovery
    - fix recovery
 
- **integratie met network device monitoring via graylog etc** 
    - https://documentation.wazuh.com/current/user-manual/capabilities/log-data-collection/how-it-works.html

## Documentation
- ensure it is correct and up-to-date
- squash commits 