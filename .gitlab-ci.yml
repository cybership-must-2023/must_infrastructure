# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence 

include: 
  - local: 'pipeline/Base.gitlab-ci.yml'
  - local: 'pipeline/Ansible.gitlab-ci.yml'
  - local: 'pipeline/Testing.gitlab-ci.yml'
  - local: 'pipeline/ssh.gitlab-ci.yml'
  - template: Security/SAST.gitlab-ci.yml

variables:
  TF_STATE_NAME:          terraform.tfstate
  TF_CACHE_KEY:           default
  TF_ROOT:                terraform

  # GitLab variables
  TF_VAR_aws_access_key_id:             "$AWS_ACCESS_KEY_ID"
  TF_VAR_aws_secret_access_key:         "$AWS_SECRET_ACCESS_KEY"
  TF_VAR_aws_session_token:             "$AWS_SESSION_TOKEN"
  TF_VAR_aws_region:                    "$AWS_REGION"
  TF_VAR_aws_key_name:                  "$AWS_KEY_NAME"
  TF_VAR_aws_monitored_servers:         "$MONITORED_SERVERS"
  TF_VAR_aws_monitored_workstations:    "$MONITORED_WORKSTATIONS"
  TF_VAR_aws_monitored_network_devices: "$MONITORED_NETWORK_DEVICES"

  MUST_PEM:                             "$AWS_PRIVATE_KEY_MUST_BASE64"

  VAULT_PASS:                           "$ANSIBLE_VAULT_PASSWORD"
  VAULT_PASS_FILE:                      "$ANSIBLE_VAULT_PASSWORD_FILE"
  VIRUSTOTAL_API:                       "$VAULT_VIRUSTOTAL_API_KEY"
  API_PWD:                              "$VAULT_API_PASSWORD"
  API_WUI_PWD:                          "$VAULT_API_WUI_PASSWORD"
  ADMIN_PWD:                            "$VAULT_ADMIN_PASSWORD"
  SSH_KEY_LOCATION:                     "$VAULT_SSH_KEY_LOCATION"

stages:
  - test
  - validate
  - build
  - deploy
  - artifacts
  - ansible
  - wazuh
  - cleanup

terraform:validate:
  extends: .terraform:validate
  needs: []

terraform:build:
  extends: .terraform:build

terraform:deploy:
  extends: .terraform:deploy
  dependencies:
    - terraform:build

terraform:output:
  extends: .terraform:output
  dependencies:
    - terraform:deploy

ssh_key:
  extends: .ssh_key

ansible:install:
  extends: .ansible:install
  dependencies:
    - ssh_key
    - terraform:output

ansible:client_setup:
  extends: .ansible:client_setup
  dependencies:
    - ssh_key
    - terraform:output

ansible:wazuh:certificates:
  extends: .ansible:wazuh:certificates
  dependencies:
    - ssh_key
    - terraform:output

ansible:wazuh:
  extends: .ansible:wazuh
  dependencies:
    - ssh_key
    - terraform:output

terraform:destroy:
  extends: .terraform:destroy
  dependencies:
    - terraform:build
