# MUST SIEM solution with Wazuh on AWS

In this repository you will find a guide to install Wazuh on AWS through Ansible and the GitLab pipeline.
If you wish to install Ansible on your on-premise servers, skip ahead to 

## Table of Contents

1. [Description](#description)
2. [GitLab Setup](#gitlab-setup)
3. [Terraform](#terraform)
4. [Pipeline](#pipeline)
4. [Ansible](#ansible)
4. [Wazuh](#wazuh)
5. [Project status](#project-status)
6. [Known errors](#known-errors)

## Description

This pipeline starts automatically after each commit and will perform at least the following steps upon pushing to the main branch:
* Provision the AWS cloud resources and configuration of software components

The architecture follows the following principles:
* The entire environment can be destroyed and deployed with one click
* Separate services are ran on separate resources
* Use declarative code where possible
* All changes are reflected in the code
* Security is key

## GitLab setup

The following **variables** must be set in the *repository's CI/CD settings*:
* ANSIBLE_VAULT_PASSWORD (the password to lock/unlock the Ansible Vault)
* ANSIBLE_VAULT_PASSWORD_FILE (the location of the password file, which is used to encrypt/decrypt yaml-files)
* AWS_ACCESS_KEY_ID (the AWS access key ID)
* AWS_KEY_NAME (the name of the used Amazon EC2 key pair)
* AWS_PRIVATE_KEY_MUST_BASE64 (the MUST.pem private key used between the Ansible server and clients, base64 encrypted)
* AWS_REGION (the AWS region)
* AWS_SECRET_ACCESS_KEY (the AWS secret access key)
* AWS_SESSION_TOKEN (the AWS session token)
* AWS_MONITORED_NETWORK_DEVICES (the amount of network devices monitored by Wazuh)
* AWS_MONITORED_SERVERS (the amount of servers monitored by Wazuh)
* AWS_MONITORED_WORKSTATIONS (the amount of workstations monitored by Wazuh)
* VAULT_ADMIN_PASSWORD[^6] (the password for the admin user, used for logging in)
* VAULT_API_PASSWORD[^6] (the password for the wazuh user, used by the API)
* VAULT_API_WUI_PASSWORD[^6] (the password for the wazuh-wui user, used by the API)
* VAULT_VIRUSTOTAL_API_KEY (the VirusTotal API key, which will be encrypted in an Ansible Vault)

## Terraform

The infrastructure is declared in several Terraform-files in the [terraform](/terraform/) directory. 
More information on these files can be found in [/terraform/README.md](/terraform/README.md)

## Pipeline

On a commit, this pipeline will create the infrastructure specified in [terraform/](/terraform/). It runs using a (local) modified version of the [Terraform-template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Terraform.gitlab-ci.yml) provided by [Gitlab](https://docs.gitlab.com/ee/user/infrastructure/iac/).

The pipeline runs through the following **stages**:
* [test](#test)
* [validate](#validate)[^2]
* [build](#build)
* [deploy](#deploy)
* [cleanup](#cleanup)

More information on the code behind these stages can be found in [.gitlab-ci.yml](.gitlab-ci.yml) and [Base.gitlab-ci.yml](./pipeline/Base.gitlab-ci.yml).

### Test

This stage is meant for user-created tests during development and SAST. Do note that GitLab-automated SAST is currently not working.

### Validate

The **validate** stage is used to format Terraform-files in a standardized format and validate them.
The terraform:fmt job is used to rewrite Terraform configuration files to a canonical format and style.
Then, the farraform:validate job validates the configuration files in a directory, referring only to the configuration and not accessing any remote services such as remote state, provider APIs, etc. This is similar to running [`terraform fmt`](https://developer.hashicorp.com/terraform/cli/commands/fmt) and [`terraform validate`](https://developer.hashicorp.com/terraform/cli/commands/validate).[^1]

### Build

The **build** stage is used to plan the terraform configuration. For more info on the content of this configuration, see [Infrastructure](#infrastructure). It is another check to verify the Terraform configuration. It also creates a JSON-formatted plan, which is passed to the artefacts of the pipeline as plan.cache. This stage is similar to running [`terraform plan`](https://developer.hashicorp.com/terraform/cli/commands/plan).

### Deploy

The **deploy** stage is used to apply the terraform configuration, which is uploaded to an AWS environment. This job takes around **DURATION ESTIMATION** minutes to complete. This stage depends on the build-stage, and  is similar to running [`terraform apply`](https://developer.hashicorp.com/terraform/cli/commands/apply). 

### Cleanup

The **cleanup** stage contains a *manual* job to destroy the terraform environment. With the terraform:cleanup job, the environment can be destroyed with the click of a button. This will also take around **DURATION ESTIMATION** to complete. This stage depends on the build-stage, and  is similar to running [`terraform destroy`](https://developer.hashicorp.com/terraform/cli/commands/destroy). 

## Ansible

For the Ansible installation and setup instructions, see the [ansible folder](/ansible/) and the [ansible README](/ansible/README.md).

## Wazuh

The Wazuh-configuration is done via Ansible, and can be found in https://gitlab.com/cybership-must-2023/must_playbooks/. 

The following setup is created:

- A Wazuh all-in-one implementation, containing the indexer, dashboard and server
- A single Wazuh agent, a minimal Ubuntu 22.04 LTS server

Wazuh has a large number of options and possibilities. For a overview of these, [Installing & Configuring Wazuh by HackerSploit](https://youtu.be/SCG0wYGS-Mg) is a good starting point.

### Extended capabilities

Wazuh's functionality can be expanded through a large number of [settings and capabilities](https://documentation.wazuh.com/current/user-manual/capabilities/index.html).  
In this section, the extra options set in the [Ansible settings](https://gitlab.com/cybership-must-2023/must_playbooks/blob/main/MUST.wazuh-single.yml) are explained.

#### Vulnerability scanning

Vulnerability scanning has been implemented. The results can be found in the Wazuh dashboard under the Vulnerabilities Module.

Sources: 
- https://documentation.wazuh.com/current/user-manual/capabilities/vulnerability-detection/how-it-works.html
- https://documentation.wazuh.com/current/deployment-options/deploying-with-ansible/reference.html#wazuh-manager > `wazuh_manager_vulnerability_detector`

#### SCA

Security Configuration Assessment (SCA) is the process of verifying that all systems conform to a set of predefined rules regarding configuration settings and approved application usage. By default, a CIS benchmark is implemented to check different operating systems for vulnerabilities.[^4]

This functionality can be expanded by implementing keyword checks.
This is currently only implemented for showcasing purposes, but can be easily expanded by modifying the files in [the SCA folder and playbook in supporting_packages](https://gitlab.com/cybership-must-2023/must_playbooks/tree/main/supporting_packages/).

This functionality can also be expanded by implementing checks against running processes.
This is currently only implemented for showcasing purposes, but can be easily expanded by modifying the files in [the SCA folder and playbook in supporting_packages](https://gitlab.com/cybership-must-2023/must_playbooks/tree/main/supporting_packages/).

Sources:
- https://documentation.wazuh.com/current/user-manual/capabilities/sec-config-assessment/index.html
- https://documentation.wazuh.com/current/user-manual/capabilities/sec-config-assessment/use-cases.html#detecting-keyword-in-a-file
- https://documentation.wazuh.com/current/user-manual/capabilities/sec-config-assessment/use-cases.html#id3

#### File integrity monitoring

Wazuh File integrity monitoring (FIM) system watches selected files and triggers alerts when these files are modified.

Sources:
- https://documentation.wazuh.com/current/user-manual/capabilities/file-integrity/index.html
- https://documentation.wazuh.com/current/deployment-options/deploying-with-ansible/reference.html#wazuh-manager > `wazuh_manager_syscheck`
- https://documentation.wazuh.com/current/user-manual/capabilities/malware-detection/virus-total-integration.html

#### FIM and YARA

You can combine the capabilities of the Wazuh FIM module with YARA to detect malware. [^3]

Sources:
- https://documentation.wazuh.com/current/user-manual/capabilities/malware-detection/fim-yara.html
- https://documentation.wazuh.com/current/proof-of-concept-guide/detect-malware-yara-integration.html

#### System inventory

The Wazuh agents are able to collect interesting system information and store it into an SQLite database for each agent on the manager side. System inventory information is enabled by default and can be found in the "Inventory data" tab on the dashboard.

Sources:
- https://documentation.wazuh.com/current/user-manual/capabilities/syscollector.html

#### Auditing who-data in Linux

From version 3.4.0, Wazuh incorporates a new functionality that obtains the who-data information from monitored files. This information contains the user who made the changes on the monitored files and the program name or process used to carry them out.

With the [who.yml file](https://gitlab.com/cybership-must-2023/must_playbooks/blob/main/supporting_packages/who.yml), who-data auditing can be activated in the directories set in [the variable files](https://gitlab.com/cybership-must-2023/must_playbooks/tree/main/vars)

Sources:
- https://documentation.wazuh.com/current/user-manual/capabilities/auditing-whodata/who-linux.html

#### Active response

Wazuh has an active response module that helps security teams automate response actions based on specific triggers, enabling them to effectively manage security incidents.

Many functionalities are possible. The following use cases have been implemented:
- [Restart the Wazuh agent if the ossec.conf file is modified](https://documentation.wazuh.com/current/user-manual/capabilities/active-response/ar-use-cases/restarting-wazuh-agent.html)
- [Block a user account if 3 unsuccessful login attempts are made (to prevent brute forcing)](https://documentation.wazuh.com/current/user-manual/capabilities/active-response/ar-use-cases/disabling-user-account.html)
- [Block and IP address that tries to brute force their way inside via SSH](https://documentation.wazuh.com/current/user-manual/capabilities/active-response/ar-use-cases/blocking-ssh-brute-force.html#infrastructure)[^5]

Sources:
- https://documentation.wazuh.com/current/user-manual/capabilities/active-response/index.html
- https://documentation.wazuh.com/current/user-manual/capabilities/active-response/default-active-response-scripts.html

#### VirusTotal integration

Wazuh detects malicious files through an integration with VirusTotal, a powerful platform aggregating multiple antivirus products and an online scanning engine.[^2] 

Sources:
- https://documentation.wazuh.com/current/user-manual/manager/manual-integration.html#virustotal
- https://documentation.wazuh.com/current/user-manual/capabilities/malware-detection/virus-total-integration.html
- https://documentation.wazuh.com/current/proof-of-concept-guide/detect-remove-malware-virustotal.html
- https://developers.virustotal.com/reference/getting-started

## Project status
*The current state of the project*

See: [To do](/ToDo.md)

## Known errors
*List the problems and bugs that are not yet fixed in the current iteration of the project.*

[^1]: The (unused) fmt-job, which runs `terraform fmt` to [refactor your code](https://developer.hashicorp.com/terraform/cli/commands/fmt), currently always fails. It is not known what causes this, as the given error code - 3 - simply means "we don't know what the problem is". The job is currently allowed to fail, so this does not break the pipeline and only causes a minor inconvenience. 

[^2]: This component is currently not working

[^3]: Special thanks to [MiguelazoDS](https://github.com/MiguelazoDS) for help with implementing this functionality: https://github.com/wazuh/wazuh/issues/14055

[^4]: The following implementation was requested, but does not seem to be functional anymore in this version of Wazuh: https://documentation.wazuh.com/4.2/user-manual/capabilities/sec-config-assessment/use-case.html 

[^5]: Untested

[^6]: The password must have a length between 8 and 64 characters and contain at least one upper and lower case letter, a number and a symbol(.*+?-)