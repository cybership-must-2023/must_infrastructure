#!/bin/bash
password_file=$1
cd /etc/ansible/roles/wazuh-ansible/MUST_playbooks/
sudo git pull
echo "--- Testing connectivity to ansible clients ---"
sudo ansible all -m ping -i inventory

echo "--- Installing Wazuh who-data auditing ---"
sudo ansible-playbook supporting_packages/who.yml -i inventory -b -K --vault-password-file $password_file
