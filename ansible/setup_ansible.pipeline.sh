#!/bin/bash
# hostname
sudo hostnamectl set-hostname Ansible-server
echo "--- Setting timezone to Africa/Kampala ---"
sudo timedatectl set-timezone Africa/Kampala

# ansible installation
echo '--- Installing required dependencies ---'
sudo apt-get update -y
sudo apt-get install lsb-release software-properties-common -y

echo '--- Setup ansible repository ---'
sudo apt-add-repository -y ppa:ansible/ansible
sudo apt-get update -y
    
echo '--- Installing ansible ---'
sudo apt-get install ansible -y

echo '--- Installing ansible-lint ---'
sudo apt install yamllint ansible-lint -y

# Move MUST.pem

cd ~
sudo mv /home/ubuntu/.ssh/MUST.pem /root/.ssh/MUST.pem

echo "--- Setting correct permissions ---"
sudo chmod 700 .ssh
sudo chmod 600 ~/.ssh/MUST.pem
sudo ls -la .ssh/

# set up Wazuh-Ansible git
echo "--- downloading Wazuh-Ansible repo ---"
cd /etc/ansible/roles/
sudo git clone --branch 4.3 https://github.com/wazuh/wazuh-ansible.git
ls

# set up MUST playbooks
echo "--- downloading MUST_playbooks ---"
cd ./wazuh-ansible/
sudo git clone https://gitlab.com/cybership-must-2023/must_playbooks.git MUST_playbooks

echo "--- Ansible setup complete ---"