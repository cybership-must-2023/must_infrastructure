# Ansible
*introduction*

Ansible is a great tool for automating the deployment of software and configuration.  
In this repository, you will find a guide on how to deploy a Wazuh environment via Ansible.

## Table of Contents

1. [Description](#description)
2. [On restarting the AWS environment](#on-restarting-the-aws-environment)
3. [Automatic installation and setup of Ansible](#automatic-installation-and-setup-of-ansible)
3. [Manual installation and setup of Ansible](#manual-installation-and-setup-of-ansible)
    1. [Ansible server](#ansible-server)
    2. [Ansible clients](#ansible-clients)
    3. [Test connection](#test-connection)
4. [Ansible vault](#ansible-vault)
4. [Ansible playbooks](#ansible-playbooks)
5. [Project status](#project-status)
6. [Known errors](#known-errors)

## Description
*description and purpose of repo*

## On restarting the AWS environment
*which resources need to be adjusted to function in the newly created environment*

When restarting the AWS environment, the IP addresses of the EC2-instances change. 
Because of this, they also need to be changed in the following files in this repository:
- the [MUST_playbook inventory file](https://github.com/Cybership-Uganda-2023/MUST_playbooks/blob/main/inventory)
- the [MUST_playbook variables file](https://github.com/Cybership-Uganda-2023/MUST_playbooks/blob/main/vars/vars-development.yml)

## Automatic installation and setup of Ansible

If this project is ran on AWS, the installation of Ansible and Wazuh are configured through the GitLab Pipeline.  
No additional setup should be required, the Wazuh-dashboard can be found at https://dashboard-dns-address.
The dns-address can be found in the artefacts of the output stage. Once the [MUST_playbook repo has been updated]( #on-restarting-the-aws-environment), execute the manual Wazuh-stage of the CI/CD pipeline.

## Manual installation and setup of Ansible

This guide follows the [official Wazuh documentation](https://documentation.wazuh.com/current/deployment-options/deploying-with-ansible/guide/install-ansible.html#remote-connection). You can do the installation following the guide, or use the scripts provided in this repository.

### Ansible server

To set up the configuration on the Ansible server, copy the contents of [setup_ansible.sh](./setup_ansible.sh).
Connect to the Ansible server via SSH and execute the following commands, copying the contents of the script into install.sh:

```Bash
sudo nano install.sh
sudo chmod +x install.sh
sudo ./install.sh
sudo rm install.sh
```

Next, the public key on the Ansible server needs to be added to the client servers' `authorized_keys` file.
Copy the last output of the script (the public SSH key).

*Alternitavely*, when on AWS, one can use [setup_ansible.AWS.sh](./setup_ansible.AWS.sh)
to use the AWS key (MUST.pem) instead of creating a new key. This is useful when working on AWS,
as this key is already in the system. When using this script, it is not required to copy the key to the Ansible clients.
Running this script will create a "popup" with nano, which prompts the user to enter the MUST.pem-private key into the file.

### Ansible clients

To ensure that the right SSH-infrastructure is present, copy the contents of [setup_client.sh](./setup_clients/setup_client.sh).
Connect to the client server via SSH and execute the following commands, copying the contents of the script into install.sh:

```Bash
sudo nano install.sh
sudo chmod +x install.sh
sudo ./install.sh
sudo rm install.sh
```

Next, the Ansible public key needs to be added to the clients configuration (*not required when using [setup_ansible.AWS.sh](./setup_ansible.AWS.sh)*).  
Open `~/.ssh/authorized_keys` and paste the contents of the [Ansible public key](#ansible-server).
Then restart the ssh-service.

```Bash
sudo nano ~/.ssh/authorized_keys
sudo systemctl restart sshd
```

### Test connection

From the ansible server, enter the following command to check if the configuration was applied successfully:

```Bash
sudo ansible Wazuh-agent -m ping
```

## Ansible vault

> Ansible Vault encrypts variables and files so you can protect sensitive content such as passwords or keys rather than leaving it visible as plaintext in playbooks or roles. To use Ansible Vault you need one or more passwords to encrypt and decrypt content. If you store your vault passwords in a third-party tool such as a secret manager, you need a script to access them. Use the passwords with the ansible-vault command-line tool to create and view encrypted variables, create encrypted files, encrypt existing files, or edit, re-key, or decrypt files. You can then place encrypted content under source control and share it more safely. ([Ansible vault documentation](https://docs.ansible.com/ansible/latest/vault_guide/vault.html))

To protect certain sensitive variables, Ansible Vault was used to encrypt files on the Ansible server. Using the [GitLab variables](/README.md#gitlab-setup) and the [ansible vault script](/ansible/setup_ansible_vault.pipeline.sh), a password file was created. Using this password file, variable files in the [/vars directory of the MUST_playbooks folder](https://github.com/Cybership-Uganda-2023/MUST_playbooks/tree/main/indexer/certificates) holding secrets can be encrypted. To execute the playbooks using these commands, `--vault-password-file /path/to/password/file` must be added.

Sources:
- https://www.redhat.com/sysadmin/ansible-playbooks-secrets
- https://docs.ansible.com/ansible/latest/vault_guide/vault_encrypting_content.html
- https://devops4solutions.com/ansible-vault-password-in-ci-cd-pipeline/
- https://www.digitalocean.com/community/tutorials/how-to-use-vault-to-protect-sensitive-ansible-data

## Ansible playbooks
*installing Wazuh with Ansible*

This section largely follows the [official Wazuh documentation](https://documentation.wazuh.com/current/deployment-options/deploying-with-ansible/installation-guide.html).

The playbook files installed by [setup_ansible.sh](./setup_ansible.MUST.sh) can be found at https://github.com/Cybership-Uganda-2023/MUST_playbooks. For more information on how to set up and install these files, consult the [README file on the MUST_playbooks repository](https://github.com/Cybership-Uganda-2023/MUST_playbooks/blob/main/README.md).

The Wazuh-dashboard can now be reached internally by using the dashboard instance's private IP, at `https://<private IP>`, and exteranally by using the dashboard instance's public DNS addressn at `https://<public DNS>`.[^1]

## Project status
*The current state of the project*

See: [To do](/ToDo.md)
 
## Known errors
*List the problems and bugs that are not yet fixed in the current iteration of the project.*

[^1]: If you recieve the following alert: `ERROR: No template found for the selected index-pattern title [wazuh-alerts-*]`, try entering the following command on the **Wazuh-manager**: `curl --silent https://raw.githubusercontent.com/wazuh/wazuh/${wazuh_major}/extensions/elasticsearch/7.x/wazuh-template.json | curl -X PUT 'https://elasticsearch_IP:9200/_template/wazuh' -H 'Content-Type: application/json' -d @- -uadmin:admin -k --silent`. (source: https://groups.google.com/g/wazuh/c/aSAIqc2_1ig)