#!/bin/bash
password_file=$1
cd /etc/ansible/roles/wazuh-ansible/MUST_playbooks/
sudo git pull
echo "--- Testing connectivity to ansible clients ---"
sudo ansible all -m ping -i inventory

echo "--- Installing Wazuh agents ---"
sudo ansible-playbook MUST.wazuh-agents-Linux.yml -i inventory -b -K --vault-password-file $password_file
