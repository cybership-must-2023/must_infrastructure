#!/bin/bash
# Reading input variables
password_file=$1
password=$2
api_key=$3
api_password=$4
api_wui_password=$5
admin_password=$6
ssh_key_location=$7

dir="/etc/ansible/roles/wazuh-ansible/MUST_playbooks/vars"
secrets_file="$dir/secrets-development.yml"
# create a password file for Ansible
echo "--- creating the Ansible password file ---"
sudo touch $password_file
sudo sh -c "echo '$password' > $password_file"

# set nano as preferred editor
echo "--- setting nano as preferred editor for Ansible"
sudo sh -c 'echo "export EDITOR=nano" >> /root/.bashrc'
sudo sh -c 'echo "export EDITOR=nano" >> /home/ubuntu/.bashrc'
sudo /bin/bash -c '. /root/.bashrc'
sudo /bin/bash -c '. /home/ubuntu/.bashrc'

# create secret variable file
echo "--- creating and encrypting secrets file ---"
cd /etc/ansible/roles/wazuh-ansible/MUST_playbooks
sudo rm $secrets_file
sudo touch $secrets_file
### Add variables before encryption
sudo /bin/bash -c "echo 'virustotal_api_key: $api_key' > $secrets_file"
sudo /bin/bash -c "echo 'api_password: $api_password' >> $secrets_file"
sudo /bin/bash -c "echo 'api_wui_password: $api_wui_password' >> $secrets_file"
sudo /bin/bash -c "echo 'admin_password: $admin_password' >> $secrets_file"
sudo /bin/bash -c "echo 'ssh_key_location: $ssh_key_location' >> $secrets_file"

###
sudo ansible-vault encrypt --vault-password-file $password_file $secrets_file

echo "--- Ansible vault setup complete ---"