#!/bin/bash
password_file=$1
cd /etc/ansible/roles/wazuh-ansible/MUST_playbooks/
sudo git pull
echo "--- Testing connectivity to ansible clients ---"
sudo ansible all -m ping -i inventory

echo "--- Setting up certificates ---"
sudo chmod +x ./indexer/certificates/wazuh-certs-tool.sh
sudo ./indexer/certificates/wazuh-certs-tool.sh -A