#!/bin/bash
password_file=$1
cd /etc/ansible/roles/wazuh-ansible/MUST_playbooks/
sudo git pull
echo "--- Testing connectivity to ansible clients ---"
sudo ansible all -m ping -i inventory

echo "--- Installing Wazuh all-in-one deployment ---"
sudo ansible-playbook MUST.wazuh-single.yml -i inventory -b -K --vault-password-file $password_file
